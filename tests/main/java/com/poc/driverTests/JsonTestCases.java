package com.poc.driverTests;

import com.poc.obd.Sensor;
import com.utility.SecureUtility;

import java.io.IOException;

public class JsonTestCases {
    public static void main(String[] args) throws IOException {
        Sensor sensor = new Sensor();
        sensor.setDistance_travelled_w_mil(123);
        System.out.println(SecureUtility.javaToJson(sensor));
    }
}
