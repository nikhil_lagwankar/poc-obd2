package com.poc.driverTests;

import com.poc.obd.Sensor;
import com.poc.sensorgenerator.RandomValueGenerator;
import com.utility.SecureUtility;

import java.io.IOException;

public class OBDSimulator {

    public static void main(String[] args) throws IOException {

        int cool_temp,rpm,rgpressure,engineoiltemp,fuelinjection,kmindicator;
        String VIN;
        Sensor OBDBlock = new Sensor();
        RandomValueGenerator r = new RandomValueGenerator();

        for(int i =0;i<25;i++)
        {
            OBDBlock.setEngine_coolant_temperature(r.cool_temp());
            OBDBlock.setEngine_rpm(r.rpm());
           OBDBlock.setFuel_rail_guage(r.rgpressure());
           OBDBlock.setEngine_oil_temperature(r.engineoiltemp());
           OBDBlock.setFuel_injection_timing(r.fuelinjectiontiming());
           OBDBlock.setDistance_travelled_w_mil(r.kmindicatorlamp());
           OBDBlock.setVehicle_identification_number(r.VIN());
            System.out.println(SecureUtility.javaToJson(OBDBlock));

        }
    }
}
