package com.poc.driverTests;

import com.poc.obd.Sensor;
import com.utility.SecureUtility;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public class HashFunctionTests {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        String s = "Why the hell";
        String hash = SecureUtility.getHash(s);
        System.out.println(hash);
    }
}
