package foo.gettingstarted.server;

import bftsmart.tom.MessageContext;
import bftsmart.tom.ServiceReplica;
import bftsmart.tom.server.defaultservices.DefaultRecoverable;
import com.utility.SecureUtility;
import foo.gettingstarted.RequestType;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

public class TreeMapServer extends DefaultRecoverable {

    Map<String, String> table;
    String databaseString;
    Connection conn;

    public TreeMapServer(int id) {
        table = new TreeMap<String, String>();
        new ServiceReplica(id, this, this);
    }

    public TreeMapServer(int id, String databaseString) {
        this.table = table;
        this.databaseString = databaseString;
        getConnection();
        new ServiceReplica(id, this, this);
    }

    public static void main(String[] args) {
        String s = "dataSource";
        if (args.length < 1) {
            System.out.println("Usage: HashMapServer <server id>");
            System.exit(0);
        }

        //reading java properties
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream("C:\\Users\\nikhi\\Principle of Computer Security\\poc-obd2\\src\\main\\resources\\config.properties");
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            int number = Integer.valueOf(args[0]) + 1;
            s = "server" + number + "connector";

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        TreeMapServer treeMapServer = new TreeMapServer(Integer.parseInt(args[0]), prop.getProperty(s));
    }

    @Override
    public byte[][] appExecuteBatch(byte[][] command, MessageContext[] mcs) {

        byte[][] replies = new byte[command.length][];
        for (int i = 0; i < command.length; i++) {
            replies[i] = executeSingle(command[i], mcs[i]);
        }

        return replies;
    }

    private byte[] executeSingle(byte[] command, MessageContext msgCtx) {
        ByteArrayInputStream in = new ByteArrayInputStream(command);
        DataInputStream dis = new DataInputStream(in);
        int reqType;
        try {
            reqType = dis.readInt();
            if (reqType == RequestType.PUT) {
                String key = dis.readUTF();
                String value = dis.readUTF();
                //String oldValue = table.put(key, value);
                String oldValue = insertData(key, value);
                byte[] resultBytes = null;
                if (oldValue != null) {
                    resultBytes = oldValue.getBytes();
                }
                return resultBytes;
            } else if (reqType == RequestType.REMOVE) {
                String key = dis.readUTF();
                String removedValue = table.remove(key);
                byte[] resultBytes = null;
                if (removedValue != null) {
                    resultBytes = removedValue.getBytes();
                }
                return resultBytes;
            } else {
                System.out.println("Unknown request type: " + reqType);
                return null;
            }
        } catch (IOException e) {
            System.out.println("Exception reading data in the replica: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public byte[] appExecuteUnordered(byte[] command, MessageContext msgCtx) {
        ByteArrayInputStream in = new ByteArrayInputStream(command);
        DataInputStream dis = new DataInputStream(in);
        int reqType;
        try {
            reqType = dis.readInt();
            if (reqType == RequestType.GET) {
                String key = dis.readUTF();
                //String readValue = table.get(key);
                String readValue = fetchData(key);
                byte[] resultBytes = null;
                if (readValue != null) {
                    resultBytes = readValue.getBytes();
                }
                return resultBytes;
            } else if (reqType == RequestType.SIZE) {
                int size = table.size();

                ByteArrayOutputStream out = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(out);
                dos.writeInt(size);
                byte[] sizeInBytes = out.toByteArray();

                return sizeInBytes;
            } else {
                System.out.println("Unknown request type: " + reqType);
                return null;
            }
        } catch (IOException e) {
            System.out.println("Exception reading data in the replica: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void installSnapshot(byte[] state) {
        ByteArrayInputStream bis = new ByteArrayInputStream(state);
        try {
            ObjectInput in = new ObjectInputStream(bis);
            table = (Map<String, String>) in.readObject();
            in.close();
            bis.close();
        } catch (ClassNotFoundException e) {
            System.out.print("Coudn't find Map: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.print("Exception installing the application state: " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public byte[] getSnapshot() {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(table);
            out.flush();
            out.close();
            bos.close();
            return bos.toByteArray();
        } catch (IOException e) {
            System.out.println("Exception when trying to take a + " +
                    "snapshot of the application state" + e.getMessage());
            e.printStackTrace();
            return new byte[0];
        }
    }

    private void getConnection() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(databaseString);
            System.out.println("Instance Connected to Database");
            //String s = "S1MV4Z45442AXG0H1";
            //fetchData(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String fetchData(String key) {
        List<String> resultString = new ArrayList<String>();
        String listStringJson = "blankList";
        try {
            Statement statement = conn.createStatement();
            String queryString = "select block from dbo.diagnosticsData where vin =" + "'" + key + "'";
            //System.out.println(queryString);
            ResultSet rs = statement.executeQuery(queryString);
            while (rs.next()) {
                //System.out.println(rs.getString(1));
                resultString.add(rs.getString(1));
            }
            listStringJson = SecureUtility.javaToJson(resultString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println(listStringJson);
        return listStringJson;
    }

    private String insertData(String key, String value) {
        List<String> resultString = new ArrayList<String>();
        String listStringJson = "blankList";
        String queryString;
        ResultSet rs;
        String hash;
        try {
            Statement statement = conn.createStatement();
            //Sensor[] sensor = (Sensor[]) SecureUtility.jsontoJava(Sensor[].class,value);
            queryString = "SELECT TOP 1 block FROM dbo.diagnosticsData where vin =" + "'" + key + "' ORDER BY pid DESC";
            //System.out.println(queryString);
            rs = statement.executeQuery(queryString);
            if (rs.next()) {
                hash = SecureUtility.getHash(rs.getString(1));
            } else {
                hash = SecureUtility.getHash("123");
            }
            //System.out.println(hash.toString());
            queryString = "insert into dbo.diagnosticsData (block, hash, vin) values (" + "'" + value + "','" + hash.toString() + "','" + key + "')";
            //System.out.println(queryString);
            rs = statement.executeQuery(queryString);
            while (rs.next()) {
                //System.out.println(rs.getString(1));
                resultString.add(rs.getString(1));
            }
            listStringJson = SecureUtility.javaToJson(resultString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println(listStringJson);
        return listStringJson;
    }
}