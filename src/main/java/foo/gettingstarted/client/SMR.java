package foo.gettingstarted.client;

import com.poc.obd.Sensor;
import com.poc.sensorgenerator.RandomValueGenerator;
import com.utility.SecureUtility;
import foo.gettingstarted.client.MapClient;

import java.io.IOException;
import java.util.Scanner;

public class SMR {

    public static void main(String[] args) throws IOException {
        String VIN;
        Sensor OBDBlock = new Sensor();
        RandomValueGenerator r = new RandomValueGenerator();
        Scanner sc = new Scanner(System.in);

        MapClient client = new MapClient(Integer.parseInt(args[0]));
        String vin = r.VIN();

        for (int i = 0; i < 5; i++) {
            OBDBlock.setEngine_coolant_temperature(r.cool_temp());
            OBDBlock.setEngine_rpm(r.rpm());
            OBDBlock.setFuel_rail_guage(r.rgpressure());
            OBDBlock.setEngine_oil_temperature(r.engineoiltemp());
            OBDBlock.setFuel_injection_timing(r.fuelinjectiontiming());
            OBDBlock.setDistance_travelled_w_mil(r.kmindicatorlamp());
            OBDBlock.setVehicle_identification_number(vin);
            //System.out.println(SecureUtility.javaToJson(OBDBlock));
            String result = client.put(vin, SecureUtility.javaToJson(OBDBlock));
        }

        while (true) {
            System.out.println("Reading value from the map");
            System.out.println("Enter the key:");
            String key =  sc.next();
            String result =  client.get(key);
            System.out.println("Value read: " + result);
        }
    }
}
