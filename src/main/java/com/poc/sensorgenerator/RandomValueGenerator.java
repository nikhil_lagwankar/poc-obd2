package com.poc.sensorgenerator;

import com.poc.obd.Sensor;

import java.util.Random;

/**
 * created by Nikhil Lagwankar on 02-12-2017
 */

public class RandomValueGenerator {


    public int cool_temp() {
        Random rand = new Random();

        // System.out.println(rand.nextInt());
        int temp1 = rand.nextInt() & Integer.MAX_VALUE;
        temp1 = temp1 % 20;
        if (temp1 == 1 || temp1 == 10 || temp1 == 20 || temp1 == 15)
            if (temp1 == 1 || temp1 == 20 || temp1 == 15)
                temp1 = 240 + (rand.nextInt() & Integer.MAX_VALUE) % 10;
            else
                temp1 = 0 - (rand.nextInt() & Integer.MAX_VALUE) % 10;
        else
            temp1 = (rand.nextInt() & Integer.MAX_VALUE) % 140 + 50;
        return temp1;
        //System.out.print("Coolant Temp: "+temp1);


    }

    public int rpm() {
        Random rand = new Random();
        int rpm = rand.nextInt() & Integer.MAX_VALUE;
        rpm = rpm % 25;
        if (rpm == 15 || rpm == 24)
            rpm = 16383 + (rand.nextInt() & Integer.MAX_VALUE) % 20;
        else
            rpm = 100 + (rand.nextInt() & Integer.MAX_VALUE) % 16183;
        //System.out.print("\tRPM: " + rpm);
        return rpm;
    }

    public int rgpressure() {
        Random rand = new Random();
        int rgpressure = rand.nextInt() & Integer.MAX_VALUE;
        rgpressure = rgpressure % 25;
        if (rgpressure == 15 || rgpressure == 24)
            rgpressure = 655350 + (rand.nextInt() & Integer.MAX_VALUE) % 20;
        else
            rgpressure = 100 + (rand.nextInt() & Integer.MAX_VALUE) % 655150;
        //System.out.print("\tRG Pressure: " + rgpressure);
        return rgpressure;
    }

    public int engineoiltemp() {
        Random rand = new Random();
        int temp1 = rand.nextInt() & Integer.MAX_VALUE;
        temp1 = temp1 % 20;
        if (temp1 == 1 || temp1 == 10 || temp1 == 20 || temp1 == 15)
            if (temp1 == 1 || temp1 == 20 || temp1 == 15)
                temp1 = 240 + (rand.nextInt() & Integer.MAX_VALUE) % 10;
            else
                temp1 = (-40) - (rand.nextInt() & Integer.MAX_VALUE) % 20;
        else
            temp1 = (rand.nextInt() & Integer.MAX_VALUE) % 200 + 10;
        //System.out.print("\tEngine oil temperature: " + temp1);
        return temp1;
    }

    public int fuelinjectiontiming() {
        Random rand = new Random();
        int fuelinjection = rand.nextInt() & Integer.MAX_VALUE;
        fuelinjection = fuelinjection % 20;
        if (fuelinjection == 1 || fuelinjection == 10 || fuelinjection == 20 || fuelinjection == 15)
            if (fuelinjection == 1 || fuelinjection == 20 || fuelinjection == 15)
                fuelinjection = 301 + (rand.nextInt() & Integer.MAX_VALUE) % 15;
            else
                fuelinjection = (-210) - (rand.nextInt() & Integer.MAX_VALUE) % 20;
        else
            fuelinjection = (rand.nextInt() & Integer.MAX_VALUE) % 300 + 10;
        //System.out.print("\tFuel Injection Timing:" + fuelinjection);
        return fuelinjection;
    }

    public int enginefuelrate() {
        Random rand = new Random();
        int enginefuelrate = rand.nextInt() & Integer.MAX_VALUE;
        enginefuelrate = enginefuelrate % 25;
        if (enginefuelrate == 15 || enginefuelrate == 24)
            enginefuelrate = 3276 + (rand.nextInt() & Integer.MAX_VALUE) % 20;
        else
            enginefuelrate = 100 + (rand.nextInt() & Integer.MAX_VALUE) % 3176;
        //System.out.print("\tenginefuelrate: " + enginefuelrate);
        return enginefuelrate;
    }

    public int kmindicatorlamp() {
        Random rand = new Random();
        int kmindicatorlamp = rand.nextInt() & Integer.MAX_VALUE;
        kmindicatorlamp = kmindicatorlamp % 25;
        if (kmindicatorlamp == 15 || kmindicatorlamp == 24)
            kmindicatorlamp = 65535 + (rand.nextInt() & Integer.MAX_VALUE) % 20;
        else
            kmindicatorlamp =
                    (rand.nextInt() & Integer.MAX_VALUE) % 65535;
        //System.out.print("\tkmindicatorlamp: " + kmindicatorlamp);
        return kmindicatorlamp;
    }

    public String VIN() {
        char alphanum[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        Random rand = new Random();
        char[] VIN = new char[17];
        for (int i = 0; i < 17; i++)
            VIN[i] = alphanum[(rand.nextInt() & Integer.MAX_VALUE) % 35];

        //System.out.println("\nVIN:");
        //System.out.println(String.valueOf(VIN));
        return String.valueOf(VIN);
    }
}
