package com.poc.obd;

/**
 * created by Nikhil Lagwankar on 02-12-2017
 * */

public class Sensor {
    int engine_coolant_temperature;
    int engine_rpm;
    int fuel_rail_guage;
    int engine_oil_temperature;
    int fuel_injection_timing;
    int engine_fuel_rate;
    String vehicle_identification_number;
    int distance_travelled_w_mil;



    public Sensor(int engine_coolant_temperature, int engine_rpm, int fuel_rail_guage, int engine_oil_temperature, int fuel_injection_timing, int engine_fuel_rate, String vehicle_identification_number, int distance_travelled_w_mil) {
        this.engine_coolant_temperature = engine_coolant_temperature;
        this.engine_rpm = engine_rpm;
        this.fuel_rail_guage = fuel_rail_guage;
        this.engine_oil_temperature = engine_oil_temperature;
        this.fuel_injection_timing = fuel_injection_timing;
        this.engine_fuel_rate = engine_fuel_rate;
        this.vehicle_identification_number = vehicle_identification_number;
        this.distance_travelled_w_mil = distance_travelled_w_mil;
    }

    public Sensor() {

    }

    public int getEngine_coolant_temperature() {
        return engine_coolant_temperature;
    }

    public void setEngine_coolant_temperature(int engine_coolant_temperature) {
        this.engine_coolant_temperature = engine_coolant_temperature;
    }

    public int getEngine_rpm() {
        return engine_rpm;
    }

    public void setEngine_rpm(int engine_rpm) {
        this.engine_rpm = engine_rpm;
    }

    public int getFuel_rail_guage() {
        return fuel_rail_guage;
    }

    public void setFuel_rail_guage(int fuel_rail_guage) {
        this.fuel_rail_guage = fuel_rail_guage;
    }

    public int getEngine_oil_temperature() {
        return engine_oil_temperature;
    }

    public void setEngine_oil_temperature(int engine_oil_temperature) {
        this.engine_oil_temperature = engine_oil_temperature;
    }

    public int getFuel_injection_timing() {
        return fuel_injection_timing;
    }

    public void setFuel_injection_timing(int fuel_injection_timing) {
        this.fuel_injection_timing = fuel_injection_timing;
    }

    public int getEngine_fuel_rate() {
        return engine_fuel_rate;
    }

    public void setEngine_fuel_rate(int engine_fuel_rate) {
        this.engine_fuel_rate = engine_fuel_rate;
    }

    public String getVehicle_identification_number() {
        return vehicle_identification_number;
    }

    public void setVehicle_identification_number(String vehicle_identification_number) {
        this.vehicle_identification_number = vehicle_identification_number;
    }

    public int getDistance_travelled_w_mil() {
        return distance_travelled_w_mil;
    }

    public void setDistance_travelled_w_mil(int distance_travelled_w_mil) {
        this.distance_travelled_w_mil = distance_travelled_w_mil;
    }
}
