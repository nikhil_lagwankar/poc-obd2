package com.utility;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SecureUtility {
    public static <T> Object jsontoJava(Class<T[]> tClass, String s) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(s, tClass);
    }

    public static String javaToJson(Object obj) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }

    public static String getHash(String key) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance( "SHA-256" );
        md.update( key.getBytes( StandardCharsets.UTF_8 ) );
        byte[] digest = md.digest();
        return String.format( "%064x", new BigInteger( 1, digest ) );
    }
}
